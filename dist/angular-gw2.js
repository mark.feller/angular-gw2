//====================================================================================================================
// Module:    angular-gw2
// Optimized: Yes
// File:      src/angular-gw2.js
//====================================================================================================================

(function (module) {

  "use strict";


//--------------------------------------------------------------------------------------------------------------------
// File: src/directives/compile.js
//--------------------------------------------------------------------------------------------------------------------

  module
    .directive('compile', ['$compile', function ($compile) {
      return function(scope, element, attrs) {
        scope.$watch(
          function(scope) {
            return scope.$eval(attrs.compile);
          },
          function(value) {
            element.html(value);
            $compile(element.contents())(scope);
          }
        );
      };
    }])

//--------------------------------------------------------------------------------------------------------------------
// File: src/directives/ngGold.js
//--------------------------------------------------------------------------------------------------------------------

  module
    .directive('ngGold', function() {
      return {
        restrict: 'AE',
        scope: {
          value: '@',
          gold: '@'
        },
        templateUrl: 'template/ngGold.html',
        controller: 'GoldCtrl',
        link: function(scope, element, attrs, ctrl) {
          attrs.$observe('value', function(val) {
            scope.parseValue(val);
          });
        }
      };
    });

//--------------------------------------------------------------------------------------------------------------------
// File: src/directives/ngItem.js
//--------------------------------------------------------------------------------------------------------------------

  module
    .directive('ngItem', function() {
      return {
        restrict: 'AE',
        scope: {
          item_id: '@'
        },
        templateUrl: 'template/ngItem.html',
        controller: 'ItemCtrl',
        link: function(scope, element, attrs, ctrl) {
          attrs.$observe('itemId', function(id) {
            scope.initItem(id);
          });
        }
      }
    });

//--------------------------------------------------------------------------------------------------------------------
// File: src/directives/ngTooltip.js
//--------------------------------------------------------------------------------------------------------------------

  module
    .directive('ngTooltip', function() {
      return {
        restrict: "AE",
        controller: "ItemCtrl",
        templateUrl: 'template/ngTooltip.html',
        transclude: true,
        scope: {
          itemId: "@item_id"
        },
        link: function(scope, element, attrs){
          var hidden;

              if (!scope.items) scope.initItem(attrs.itemId);

          element
            .on('mouseenter', function(){
              scope.hidden = false;
              scope.$digest();
            })
            .on('mouseleave', function(){scope.hidden = true; scope.$digest();});
        }
      }
    });

//--------------------------------------------------------------------------------------------------------------------
// File: src/services/items.js
//--------------------------------------------------------------------------------------------------------------------

  module
    .factory('Items', ['$resource', function($resource) {
      return $resource('https://api.guildwars2.com/:version/items/:id',
                       {version: 'v2'});
    }])

//--------------------------------------------------------------------------------------------------------------------
// File: src/controllers/GoldCtrl.js
//--------------------------------------------------------------------------------------------------------------------

  module
    .controller('GoldCtrl', ['$scope', function($scope) {
      $scope.parseValue = function(value) {
        var neg = value<0;
        value = Math.abs(value);

        $scope.gold = {
          copper: Math.floor(value%100),
          silver: Math.floor((value%10000-value%100)/100),
          gold: Math.floor(value/10000),
          neg: neg
        };
      };
    }])

//--------------------------------------------------------------------------------------------------------------------
// File: src/controllers/ItemCtrl.js
//--------------------------------------------------------------------------------------------------------------------

  module
    .controller('ItemCtrl', ['$scope', 'Items', function ($scope, Items) {
      var addNameStyle = function(item) {
        item.nameStyle = {color:rarityColors(item.rarity)};
        return item;
      }

      var rarityColors = function(rarity) {
        if      (rarity=="Junk"       || rarity==0) { return "#aaa"    }
        else if (rarity=="Basic"      || rarity==1) { return "#000"    }
        else if (rarity=="Fine"       || rarity==2) { return "#62a4da" }
        else if (rarity=="Masterwork" || rarity==3) { return "#1a9306" }
        else if (rarity=="Rare"       || rarity==4) { return "#fcd00b" }
        else if (rarity=="Exotic"     || rarity==5) { return "#ffa405" }
        else if (rarity=="Ascended"   || rarity==6) { return "#fb3e8d" }
        else if (rarity=="Legendary"  || rarity==7) { return "#4C139D" }
        else {
          return "";
        }
      }

      var addSuffix = function(item) {
        if (item.details.suffix_item_id) {
          Items.get({id:item.details.suffix_item_id}).$promise
            .then(function(suffix) {
              item.suffix = suffix;
              $scope.item = item;
              $scope.ready = true;
              return item;
            });
        } else {
          $scope.item = item;
          $scope.ready = true;
          return item;
        }
      }

      $scope.flagToString = function(flag) {
        if (flag=="SoulBindOnUse") { return "Soulbound On Use" }
      }

      $scope.initItem = function(itemid) {
        Items.get({id:itemid}).$promise
          .then(addNameStyle)
          .then(addSuffix);
      }

      $scope.ready = false;
    }]);

//--------------------------------------------------------------------------------------------------------------------
// File: .tmp/templates.js
//--------------------------------------------------------------------------------------------------------------------

  module.run(['$templateCache', function($templateCache) {
    'use strict';

    $templateCache.put('template/ngGold.html',
      "<div ng-style=\"{color:gold.neg?'#dd2222':''}\"><span ng-show=\"gold.neg\">-</span> <span ng-show=\"gold.gold\">{{gold.gold}} <img src=\"https://wiki.guildwars2.com/images/d/d1/Gold_coin.png\"></span> <span ng-show=\"gold.silver\">{{gold.silver}} <img src=\"https://wiki.guildwars2.com/images/3/3c/Silver_coin.png\"></span> <span ng-show=\"gold.copper\">{{gold.copper}} <img src=\"https://wiki.guildwars2.com/images/e/eb/Copper_coin.png\"></span></div>"
    );


    $templateCache.put('template/ngItem.html',
      "<div class=\"gw2-item-tooltip\"><img class=\"item-icon\" width=\"55px\" height=\"55px\" ng-src=\"{{item.icon}}\"><ng:switch on=\"ready\"><div ng:switch-when=\"false\"><div class=\"description\">Loading data...</div></div><div class=\"animate-switch\" ng:switch-default><div class=\"description\"><span ng-style=\"item.nameStyle\"><b>{{item.name}}</b></span><br><div class=\"info\"><span ng-show=\"item.details.min_power\">Weapon Strength: <span class=\"tooltipGreen\">{{item.details.min_power}} - {{item.details.max_power}}</span></span> <span ng-show=\"item.details.defense\">Defense: <span class=\"tooltipGreen\">{{item.details.defense}}</span></span><ul class=\"stats\"><li ng-repeat=\"stat in item.details.infix_upgrade.attributes\">+{{stat.modifier}} {{stat.attribute}}</li></ul><div ng-show=\"item.suffix\" class=\"socket\"><img width=\"16px\" height=\"16px\" ng-src=\"{{item.suffix.icon}}\"> {{item.suffix.name}}<ul><li ng-repeat=\"bonus in item.suffix.details.bonuses\">({{$index+1}}) <span compile=\"bonus\"></span></li><li ng-repeat=\"bonus in item.suffix.details.infix_upgrade.buff\" compile=\"bonus\"></li></ul></div>{{item.details.type}}<br><span ng-show=\"item.details.weight_class\">{{item.details.weight_class}} {{item.type}}<br></span> <span ng-show=\"item.details.damage_type\" class=\"damageType\">Damage Type: {{item.details.damage_type}}<br></span> Required Level: {{item.level}}<br><span ng-repeat=\"flag in item.flags\">{{flagToString(flag)}}</span><br></div></div></div></ng:switch></div>"
    );


    $templateCache.put('template/ngTooltip.html',
      "<div class=\"gw2-tooltip\"><div ng-include=\"'template/ngItem.html'\"></div></div>"
    );

  }]);


}) (angular.module ('angular-gw2', []));


