angular.module('angular-gw2')
  .directive('ngGold', function() {
    return {
      restrict: 'AE',
      scope: {
        value: '@',
        gold: '@'
      },
      templateUrl: 'template/ngGold.html',
      controller: 'GoldCtrl',
      link: function(scope, element, attrs, ctrl) {
        attrs.$observe('value', function(val) {
          scope.parseValue(val);
        });
      }
    };
  });
