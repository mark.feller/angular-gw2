angular.module('angular-gw2')
  .directive('ngItem', function() {
    return {
      restrict: 'AE',
      scope: {
        item_id: '@'
      },
      templateUrl: 'template/ngItem.html',
      controller: 'ItemCtrl',
      link: function(scope, element, attrs, ctrl) {
        attrs.$observe('itemId', function(id) {
          scope.initItem(id);
        });
      }
    }
  });
