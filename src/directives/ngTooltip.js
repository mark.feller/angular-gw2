angular.module('angular-gw2')
  .directive('ngTooltip', function() {
    return {
      restrict: "AE",
      controller: "ItemCtrl",
      templateUrl: 'template/ngTooltip.html',
      transclude: true,
      scope: {
        itemId: "@item_id"
      },
      link: function(scope, element, attrs){
        var hidden;

            if (!scope.items) scope.initItem(attrs.itemId);

        element
          .on('mouseenter', function(){
            scope.hidden = false;
            scope.$digest();
          })
          .on('mouseleave', function(){scope.hidden = true; scope.$digest();});
      }
    }
  });
