angular.module('angular-gw2')
  .controller('ItemCtrl', ['$scope', 'Items', function ($scope, Items) {
    var addNameStyle = function(item) {
      item.nameStyle = {color:rarityColors(item.rarity)};
      return item;
    }

    var rarityColors = function(rarity) {
      if      (rarity=="Junk"       || rarity==0) { return "#aaa"    }
      else if (rarity=="Basic"      || rarity==1) { return "#000"    }
      else if (rarity=="Fine"       || rarity==2) { return "#62a4da" }
      else if (rarity=="Masterwork" || rarity==3) { return "#1a9306" }
      else if (rarity=="Rare"       || rarity==4) { return "#fcd00b" }
      else if (rarity=="Exotic"     || rarity==5) { return "#ffa405" }
      else if (rarity=="Ascended"   || rarity==6) { return "#fb3e8d" }
      else if (rarity=="Legendary"  || rarity==7) { return "#4C139D" }
      else {
        return "";
      }
    }

    var addSuffix = function(item) {
      if (item.details.suffix_item_id) {
        Items.get({id:item.details.suffix_item_id}).$promise
          .then(function(suffix) {
            item.suffix = suffix;
            $scope.item = item;
            $scope.ready = true;
            return item;
          });
      } else {
        $scope.item = item;
        $scope.ready = true;
        return item;
      }
    }

    $scope.flagToString = function(flag) {
      if (flag=="SoulBindOnUse") { return "Soulbound On Use" }
    }

    $scope.initItem = function(itemid) {
      Items.get({id:itemid}).$promise
        .then(addNameStyle)
        .then(addSuffix);
    }

    $scope.ready = false;
  }]);
