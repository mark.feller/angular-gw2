angular.module('angular-gw2')
  .controller('GoldCtrl', ['$scope', function($scope) {
    $scope.parseValue = function(value) {
      var neg = value<0;
      value = Math.abs(value);

      $scope.gold = {
        copper: Math.floor(value%100),
        silver: Math.floor((value%10000-value%100)/100),
        gold: Math.floor(value/10000),
        neg: neg
      };
    };
  }])
