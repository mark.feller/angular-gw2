angular.module('angular-gw2')
  .factory('Items', ['$resource', function($resource) {
    return $resource('https://api.guildwars2.com/:version/items/:id',
                     {version: 'v2'});
  }])
