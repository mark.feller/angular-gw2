/*global module:false*/
module.exports = function(grunt) {

  grunt.initConfig({

    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! \n * <%= pkg.title || pkg.name %> v<%= pkg.version %>\n' +
      ' * <%= pkg.homepage %>\n' +
      ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
      ' * License: <%= pkg.license %>\n' +
      ' */\n',

    // Task configuration.
    clean: {
      build: {
        src: ["dist/*"]
      },
      tmp: {
        src: [".tmp/*"]
      }
    },

    uglify: {
      options: {
        report: 'gzip',
        mangle: false
      },
      build: {
        src: 'dist/angular-gw2.js',
        dest: 'dist/angular-gw2.min.js'
      }
    },

    jshint: {
      jshintrc: '.jshintrc',
      gruntfile: {
        src: 'Gruntfile.js'
      },
      src: {
        src: ['src/*.js']
      }
    },

    concat: {
      build: {
        options: {
        },
        files: {
          '.tmp/angular-gw2.js':  [
            'src/angular-gw2.js',
            'src/directives/*.js',
            'src/services/*.js',
            'src/controllers/*.js'
          ],
        }
      }
    },

    ngtemplates: {
      'angular-gw2': {
        src: 'template/*.html',
        dest:'.tmp/templates.js',
        options:    {
          htmlmin:  { collapseWhitespace: true, collapseBooleanAttributes: true },
        }
      }
    },

    'angular-builder': {
      options: {
        mainModule: 'angular-gw2'
      },
      app: {
        src: [
          'src/angular-gw2.js',
          'src/directives/*.js',
          'src/services/*.js',
          'src/controllers/*.js',
          '.tmp/templates.js'
        ],
        dest: 'dist/angular-gw2.js'
      }
    },

    less: {
      development: {
        options: {
          paths: ["assets/css"]
        },
        files: {
          "dist/angular-gw2.css": "css/less/item.less"
        }
      }
    },

    cssmin: {
      options: {
      },
      target: {
        files: {
          'dist/angular-gw2.min.css': 'dist/angular-gw2.css'
        }
      }
    },

    copy: {
      main: {
        expand: true,
        cwd: 'css/',
        src: 'images/*',
        dest: 'dist/images/',
        flatten: true,
        filter: 'isFile',
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-angular-builder');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', [
    'clean',
    'ngtemplates',
    'concat',
    'angular-builder',
    'uglify',
    'less',
    'cssmin',
    'copy'
  ]);
  grunt.registerTask('build', ['default']);

};
