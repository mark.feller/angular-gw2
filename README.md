# Angular Directives for Guild Wars 2

## Installation

#### Install with Bower
```sh
$ bower install angular-gw2
```

Once you are done downloading all the dependencies and project files the only remaining part is to add dependencies on the `angular-gw2` AngularJS module:

```js
angular.module('myModule', ['angular-gw2']);
```

## Contributing to the Project

Contributions are more than welcome. This is currently a side project of mine to help me learn the inner workings of Angular and any code/advice would be welcome.
